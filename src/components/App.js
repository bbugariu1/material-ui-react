import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/styles'
import Header from './ui/Header';
import theme from './ui/theme';
import Footer from './ui/Footer';
import Typography from '@material-ui/core/Typography';
import LandingPage from "./pages/landingpage/LandingPage";

function App() {
  const [tab, setTab] = React.useState(0);
  const [selectedIndex, setSelectedIndex] = React.useState(0);

  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter>
        <Header tab={tab} setTab={setTab} selectedIndex={selectedIndex} setSelectedIndex={setSelectedIndex}/>

        {/*<Container maxWidth={"lg"}>*/}
            <Switch>
              <Route exact path="/" component={LandingPage} />
              <Route exact path="/services" component={() => <Typography variant="h1">Services</Typography>} />
              <Route exact path="/customservice" component={() => <Typography variant="h1">Custm service</Typography>} />
              <Route exact path="/mobileapps" component={() => <Typography variant="h1">Mobile apps development</Typography>} />
              <Route exact path="/websites" component={() => <Typography variant="h1">Websites</Typography>} />
              <Route exact path="/revolution" component={() => <Typography variant="h1">Revolution</Typography>} />
              <Route exact path="/about" component={() => <Typography variant="h1">About us</Typography>} />
              <Route exact path="/contact" component={() => <Typography variant="h1">Contact</Typography>} />
              <Route exact path="/estimates" component={() => <Typography variant="h1">Estimates</Typography>} />
            </Switch>
        {/*</Container>*/}

        <Footer tab={tab} setTab={setTab} selectedIndex={selectedIndex} setSelectedIndex={setSelectedIndex}/>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
