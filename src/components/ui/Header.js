import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import { makeStyles, Tabs, Tab, Button, Menu, MenuItem, useMediaQuery, useTheme, SwipeableDrawer, IconButton } from '@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import MenuIcon from '@material-ui/icons/Menu';
import { Link } from "react-router-dom"

import logo from '../../assets/logo.svg';

function ElevationScroll(props) {
  const { children } = props;
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0
  });

  return React.cloneElement(children, {
    elevation: trigger ? 4 : 0,
  });
}

const useStyles = makeStyles(theme => ({
  toolbarMargin: {
    ...theme.mixins.toolbar,
    marginBottom: "3em",
    [theme.breakpoints.down('md')]: {
      marginBottom: "2em"
    },
    [theme.breakpoints.down('xs')]: {
      marginBottom: "1.25em"
    }
  },
  logo: {
    height: "8em",
    [theme.breakpoints.down('md')]: {
      height: "7em",
    },
    [theme.breakpoints.down('xs')]: {
      height: "5.5em"
    }
  },
  logoContainer: {
    padding: 0,
    "&:hover": {
      backgroundColor: "transparent"
    }
  },
  tabsContainer: {
    marginLeft: "auto"
  },
  tab: {
    ...theme.typography.tab,
    minWidth: 10,
    [theme.breakpoints.down('md')]: {
      marginLeft: 0
    },
    marginLeft: "25px"
  },
  button: {
    ...theme.typography.estimate,
    borderRadius: "50px",
    marginLeft: "50px",
    marginRight: "25px",
    height: "45px",
    "&:hover": {
      backgroundColor: theme.palette.secondary.light
    }
  },
  menu: {
    backgroundColor: theme.palette.common.blue,
    color: "white",
    borderRadius: 0,
    zIndex: 1302
  },
  menuItem: {
    ...theme.typography.tab,
    opacity: 0.7,
    "&:hover": {
      opacity: 1,
    }
  },
  selected: {
    opacity: 1,
  },
  drawerIconContainer: {
    marginLeft: "auto",
    "&:hover": {
      backgroundColor: "transparent",
    }
  },
  drawerIcon: {
    width: "50px",
    height: "50px"
  },
  drawer: {
    backgroundColor: theme.palette.common.blue
  },
  drawerItem: {
    ...theme.typography.tab,
    color: "white",
    opacity: 0.7
  },
  drawerItemSelected: {
    "& .MuiListItemText-root" : {
      opacity: 1
    }
  },
  drawerItemEstimates: {
    backgroundColor: theme.palette.common.orange
  },
  appBar: {
    zIndex: theme.zIndex.modal + 1
  }
}));

const Header = (props) => {
  const classes = useStyles();
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down('md'));
  const iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent);

  const [anchorEl, setAnchorEl] = React.useState(null);
  const [openMenu, setOpenMenu] = React.useState(false);
  const [openDrawer, setOpenDrawer] = React.useState(false);

  const menuOptions = [
    { name: "Services", link: "/services", activeIndex: 1, selectedIndex: 0 },
    { name: "Custom Service Development", link: "/customservice", activeIndex: 1, selectedIndex: 1 },
    { name: "IOS/Android Dvelopment", link: "/mobileapps", activeIndex: 1, selectedIndex: 2 },
    { name: "Website Development", link: "/websites", activeIndex: 1, selectedIndex: 3 }
  ];

  const routes = [
    { name: "Home", link: "/", activeIndex: 0 },
    {
      name: "Service",
      link: "/services",
      activeIndex: 1,
      ariaOwns: anchorEl ? "simple-menu" : undefined,
      ariaHasPopup: anchorEl ? "simple-menu" : undefined,
      mouseOver: (event) => handleClick(event)
    },
    { name: "The Revolution", link: "/revolution", activeIndex: 2 },
    { name: "About Us", link: "/about", activeIndex: 3 },
    { name: "Contact Us", link: "/contact", activeIndex: 4 },
    { name: "Free estimates", link: "/estimates", activeIndex: 5 }
  ];

  React.useEffect(() => {
    [...routes, ...menuOptions].forEach((route => {
      switch (window.location.pathname) {
        case `${route.link}`:
          if (props.tab !== route.activeIndex) {
            props.setTab(route.activeIndex);
            if (route.selectedIndex && route.selectedIndex !== props.selectedIndex) {
              props.setSelectedIndex(route.selectedIndex);
            }
          }
          break;
        default:
          break;
      }
    }));
  }, [props.tab, menuOptions, props.selectedIndex, routes, props]);

  const handleChange = (e, tab) => props.setTab(tab);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
    setOpenMenu(true);
  }

  const handleClose = () => {
    setAnchorEl(null);
    setOpenMenu(false);
  };

  const tabs = (
    <>
      <Tabs value={props.tab} onChange={handleChange} className={classes.tabsContainer} indicatorColor="primary">
        {routes.map((route, index) => (
          <Tab
            key={route.name}
            className={classes.tab}
            label={route.name}
            component={Link}
            to={route.link}
            aria-owns={route.ariaOwns}
            aria-haspopup={route.ariaHasPopup}
            onMouseOver={route.mouseOver}
          />
        ))}
      </Tabs>
      <Button variant="contained" color="secondary" className={classes.button}>Free Estimate</Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        open={openMenu}
        onClose={handleClose}
        classes={{ paper: classes.menu }}
        MenuListProps={{ onMouseLeave: handleClose }}
        elevation={0}
        keepMounted
        style={{zIndex: theme.zIndex.modal + 2}}
      >
        {menuOptions.map((menuItem, index) => (
          <MenuItem
            key={index}
            onClick={() => { handleClose(); props.setTab(1); props.setSelectedIndex(index) }}
            component={Link}
            selected={index === props.selectedIndex && props.tab === 1}
            to={menuItem.link}
            classes={{ root: classes.menuItem, selected: classes.selected }}
          >
            {menuItem.name}
          </MenuItem>
        ))}
      </Menu>
    </>
  );

  const list = (
    <List disablePadding>
      {routes.map(route => {
        return (
          <ListItem
            key={route.name}
            divider
            button
            onClick={() => { props.setTab(route.activeIndex); setOpenDrawer(false); }}
            component={Link}
            to={route.link}
            selected={props.tab === route.activeIndex}
            classes={{selected: classes.drawerItemSelected}}
          >
            <ListItemText
              className={classes.drawerItem}
              disableTypography
            >
              {route.name}
            </ListItemText>
          </ListItem>
        );
      })}
    </List>
  );

  const drawer = (
    <>
      <SwipeableDrawer
        disableBackdropTransition={!iOS}
        disableDiscovery={iOS}
        open={openDrawer}
        onOpen={() => setOpenDrawer(true)}
        onClose={() => setOpenDrawer(false)}
        classes={{
          paper: classes.drawer
        }}>
        <div className={classes.toolbarMargin} />
        {list}
      </SwipeableDrawer>
      <IconButton disableRipple className={classes.drawerIconContainer} onClick={e => setOpenDrawer(!openDrawer)}>
        <MenuIcon className={classes.drawerIcon} />
      </IconButton>
    </>
  );

  return (
    <>
      <ElevationScroll {...props}>
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar disableGutters>
            <Button disableRipple component={Link} to="/" className={classes.logoContainer} onClick={() => props.setTab(0)}>
              <img src={logo} alt="company logo" className={classes.logo} />
            </Button>
            {matches ? drawer : tabs}
          </Toolbar>
        </AppBar>
      </ElevationScroll>
      <div className={classes.toolbarMargin} />
    </>
  );
}

export default Header;