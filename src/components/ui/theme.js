import { createMuiTheme } from '@material-ui/core/styles';

const arcBlue = '#0B72B9';
const arcOrange = '#FFBA60';
const arcGray = '#868686';
const fonts = {
    raleway: "Raleway",
    pacifico: "Pacifico",
    roboto: "Roboto"
}

export default createMuiTheme({
    palette: {
        common: {
            blue: `${arcBlue}`,
            orange: `${arcOrange}`
        },
        primary: {
            main: `${arcBlue}`
        },
        secondary: {
            main: `${arcOrange}`
        }
    },
    typography: {
        tab: {
            fontFamily: fonts.raleway,
            textTransform: "none",
            fontWeight: 700,
            fontSize: "1rem",
        },
        estimate: {
            fontFamily: fonts.pacifico,
            textTransform: "capitalize",
            fontSize: "1rem",
            color: "white"
        },
        learnButton: {
            color: arcBlue,
            borderColor: arcBlue,
            borderWidth: 2,
            borderRadius: 50,
            textTransform: "none",
            fontFamily: fonts.roboto,
            fontWeight: "bold"
        },
        h2: {
            fontFamily: fonts.raleway,
            fontWeight: 700,
            fontSize: "2.5em",
            color: arcBlue,
            lineHeight: 1.5
        },
        subtitle1: {
            fontSize: "1.25em",
            fontWeight: 300,
            color: arcGray
        },
        h4: {
            fontFamily: fonts.raleway,
            fontSize: "1.75rem",
            color: arcBlue,
            fontWeight: "bold"
        },
        specialText: {
            color: arcOrange,
            fontFamily: fonts.pacifico,
        }
    }
});