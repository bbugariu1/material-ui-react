import React from "react";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import footerAdornment from "../../assets/Footer Adornment.svg";
import { Hidden } from "@material-ui/core";
import facebook from "../../assets/facebook.svg";
import instagram from "../../assets/instagram.svg";
import twitter from "../../assets/twitter.svg";

const useStyles = makeStyles(theme => ({
    footer: {
        backgroundColor: theme.palette.common.blue,
        zIndex: theme.zIndex.modal + 1,
        position: "relative",
    },
    adornment: {
        width: "25em",
        verticalAlign: "bottom",
        [theme.breakpoints.down("md")]: {
            width: "21em"
        },
        [theme.breakpoints.down("sm")]: {
            width: "15em"
        }
    },
    mainContainer: {
        position: "absolute"
    },
    link: {
        color: "white",
        fontFamily: "Arial",
        fonstSize: "0.75em",
        flexGrow: 1,
        textDecoration: "none",
    },
    gridItem: {
        margin: "3em"
    },
    socialContainer: {
        position: "absolute",
        marginTop: "-6em",
        right: "3em",
    },
    icon: {
        width: "4em",
        height: "4em",
        [theme.breakpoints.down("xs")]: {
            width: "2.5em",
            height: "2.5em",
            right: "2em"
        }
    }
}))

const Footer = (props) => {
    const classes = useStyles();

    return (
        <footer className={classes.footer}>
            <Hidden mdDown>
                <Grid container justify="center" className={classes.mainContainer}>
                    <Grid item className={classes.gridItem}>
                        <Grid container direction="column" component={Link} to="/" spacing={2}>
                            <Grid item className={classes.link} onClick={() => props.setSelectedIndex(0)}>
                                Home
                        </Grid>
                        </Grid>
                    </Grid>

                    <Grid item className={classes.gridItem}>
                        <Grid container direction="column" spacing={2}>
                            <Grid item className={classes.link} component={Link} to="/services" onClick={() => { props.setTab(1); props.setSelectedIndex(0) }}>
                                Services
                        </Grid>
                            <Grid item className={classes.link} component={Link} to="/customservice" onClick={() => { props.setTab(1); props.setSelectedIndex(0) }}>
                                Custom Software Development
                        </Grid>
                            <Grid item className={classes.link} component={Link} to="/mobileapps" onClick={() => { props.setTab(1); props.setSelectedIndex(0) }}>
                                iOS/Android App Development
                        </Grid>
                            <Grid item className={classes.link} component={Link} to="/websites" onClick={() => { props.setTab(1); props.setSelectedIndex(0) }}>
                                Website Development
                        </Grid>
                        </Grid>
                    </Grid>

                    <Grid item className={classes.gridItem}>
                        <Grid container direction="column" spacing={2}>
                            <Grid item className={classes.link} component={Link} to="/revolution" onClick={() => { props.setTab(2); props.setSelectedIndex(1) }}>
                                The Revolution
                        </Grid>
                            <Grid item className={classes.link} component={Link} to="/revolution" onClick={() => { props.setTab(2); props.setSelectedIndex(1) }}>
                                Vision
                        </Grid>
                            <Grid item className={classes.link} component={Link} to="/revolution" onClick={() => { props.setTab(2); props.setSelectedIndex(1) }}>
                                Technology
                        </Grid>
                            <Grid item className={classes.link} component={Link} to="/revolution" onClick={() => { props.setTab(2); props.setSelectedIndex(1) }}>
                                Process
                        </Grid>
                        </Grid>
                    </Grid>

                    <Grid item className={classes.gridItem}>
                        <Grid container direction="column" spacing={2}>
                            <Grid item className={classes.link} component={Link} to="/about" onClick={() => { props.setTab(3); props.setSelectedIndex(2) }}>
                                About Us
                        </Grid>
                            <Grid item className={classes.link} component={Link} to="/about" onClick={() => { props.setTab(3); props.setSelectedIndex(2) }}>
                                Mission Statement
                        </Grid>
                            <Grid item className={classes.link} component={Link} to="/about" onClick={() => { props.setTab(3); props.setSelectedIndex(2) }}>
                                History
                        </Grid>
                            <Grid item className={classes.link} component={Link} to="/about" onClick={() => { props.setTab(3); props.setSelectedIndex(2) }}>
                                Team
                        </Grid>
                        </Grid>
                    </Grid>

                    <Grid item className={classes.gridItem}>
                        <Grid container direction="column" spacing={2}>
                            <Grid item className={classes.link} component={Link} to="/contact" onClick={() => { props.setTabyarn(4); props.setSelectedIndex(3) }} selected={props.tab === props.selectedIndex}>
                                Contact Us
                        </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Hidden>
            <img src={footerAdornment} alt="footer decorative adornment" className={classes.adornment} />

            <Grid container justify="flex-end" spacing={2} className={classes.socialContainer}>
                <Grid item component={"a"} href="https://bbugariu.com" target="_blank">
                    <img src={facebook} alt="A dying old machine" className={classes.icon}/>
                </Grid>
                <Grid item component={"a"} href="https://bbugariu.com" target="_blank">
                    <img src={instagram} alt="A dying old machine" className={classes.icon}/>
                </Grid>
                <Grid item component={"a"} href="https://bbugariu.com" target="_blank">
                    <img src={twitter} alt="A dying old machine" className={classes.icon}/>
                </Grid>
            </Grid>
        </footer>
    );
}

export default Footer;