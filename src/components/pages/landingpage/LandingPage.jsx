import React from "react";
import Lottie from "react-lottie";
import makeStyles from "@material-ui/core/styles/makeStyles";
import landingAnimationData from "../../../animations/landinganimation/data"
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import ButtonArrow from "./../../ui/ButtonArrow";
import Typography from "@material-ui/core/Typography";
import useTheme from "@material-ui/core/styles/useTheme";
import customSoftwareIcon from "../../../assets/Custom Software Icon.svg";
import mobileAppsIcon from "../../../assets/mobileIcon.svg";
import websiteIcon from "../../../assets/websiteIcon.svg";
import {useMediaQuery} from "@material-ui/core";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import repeatingBackground from "../../../assets/repeatingBackground.svg";

const animationOptions = {
    loop: true,
    autoplay: true,
    animationData: landingAnimationData,
    rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice'
    }
}

const useStyles = makeStyles(theme => ({
    mainContainer: {
      marginTop: "5em",
        [theme.breakpoints.down("md")]: {
          marginTop: "3em"
        },
        [theme.breakpoints.down("sm")]: {
          marginTop: "2"
        }
    },
    animation: {
        maxWidth: "50em",
        minWidth: "21em",
        marginTop: "2em",
        marginLeft: "10%",
        [theme.breakpoints.down("sm")]: {
            maxWidth: "30em"
        }
    },
    buttonContainer: {
      marginTop: "1em"
    },
    estimate: {
        ...theme.typography.estimate,
        backgroundColor: theme.palette.common.orange,
        borderRadius: 50,
        height: 45,
        width: 145,
        marginRight: 40,
        "&:hover": {
            backgroundColor: theme.palette.secondary.light
        }
    },
    learnButtonHero: {
        ...theme.typography.learnButton,
        fontSize: "0.9em",
        height: 45,
        width: 145
    },
    learnButton: {
        ...theme.typography.learnButton,
        fontSize: "0.7em",
        height: 35,
        padding: 5,
        [theme.breakpoints.down("sm")]: {
            marginBottom: "2em"
        }
    },
    heroTextContainer: {
        minWidth: "21.5em",
        marginLeft: "1em",
        [theme.breakpoints.down("sm")]: {
            marginLeft: 0
        }
    },
    specialText: {
        ...theme.typography.specialText,
        marginLeft: theme.spacing(1)
    },
    subtitle: {
        marginBottom: "1em"
    },
    icon: {
        marginLeft: "2em",
        [theme.breakpoints.down("xs")]: {
            marginLeft: 0
        }
    },
    serviceContainer: {
        marginTop: "12em",
        marginLeft: "5em",
        marginRight: "5em",
        [theme.breakpoints.down("sm")]: {
            marginLeft: 0,
            marginRight: 0,
            textAlign: "center"
        }
    },
    cardHolder: {
        padding: "12em",
        border: "1px solid red",
        background: repeatingBackground
    }
}))

const LandingPage = () => {
    const classes = useStyles();
    const theme = useTheme();
    const matchesSM = useMediaQuery(theme.breakpoints.down("sm"));

    return (
        <Grid container direction={"column"} className={classes.mainContainer}>
            <Grid item> {/* -- Hero Block -- */}
                <Grid container direction="row" justify="flex-end" alignItems="center">
                    <Grid item sm className={classes.heroTextContainer}>
                        <Typography variant="h2" align="center">
                            Bringing West Coast Technology
                            <br />
                            to the Midwest
                        </Typography>
                        <Grid container justify="center" className={classes.buttonContainer}>
                            <Grid item>
                                <Button className={classes.estimate} variant={"contained"} disableRipple>Free Estimates</Button>
                            </Grid>
                            <Grid item>
                                <Button className={classes.learnButtonHero} color={"primary"} variant={"outlined"} disableRipple>
                                    <span style={{marginRight: 5}}>Learn More</span>
                                    <ButtonArrow class="" width={15} height={15} fill={theme.palette.common.blue}/>
                                </Button>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item sm className={classes.animation}>
                        <Lottie
                            options={animationOptions}
                            width={"100%"}
                            height={"100%"}
                        />
                    </Grid>
                </Grid>
            </Grid>

            <Grid item>
                <Grid container direction="row" justify={matchesSM ? "center" : undefined} className={classes.serviceContainer}>
                    {/* -- Custom Software Block -- */}
                    <Grid item>
                        <Typography variant="h4">
                            Custom Software Development
                        </Typography>
                        <Typography variant="subtitle1" className={classes.subtitle}>
                            Save Energy. Save Time. Save Money.
                        </Typography>
                        <Typography variant="subtitle1">
                            Complete digital solutions, from investigation
                            <br />
                            to
                            <span className={classes.specialText}>
                                celebration
                            </span>
                        </Typography>
                        <Button className={classes.learnButton} color={"primary"} variant={"outlined"} disableRipple>
                            <span style={{marginRight: 5}}>Learn More</span>
                            <ButtonArrow class="" width={15} height={15} fill={theme.palette.common.blue}/>
                        </Button>
                    </Grid>
                    <Grid item>
                        <img src={customSoftwareIcon} alt="custom software icon" className={classes.icon}/>
                    </Grid>
                </Grid>

                <Grid container direction="row" justify={matchesSM ? "center" : "flex-end"} className={classes.serviceContainer}>
                    {/* -- IOS/Android development Block -- */}
                    <Grid item>
                        <Typography variant="h4">
                            iOS/Android App Development
                        </Typography>
                        <Typography variant="subtitle1" className={classes.subtitle}>
                            Extend Functionality. Extend Access. Increase Engagement.
                        </Typography>
                        <Typography variant="subtitle1">
                            Integrate your web experience or create a standalone app
                            <br />
                            with either mobile platform.
                        </Typography>
                        <Button className={classes.learnButton} color={"primary"} variant={"outlined"} disableRipple>
                            <span style={{marginRight: 5}}>Learn More</span>
                            <ButtonArrow class="" width={15} height={15} fill={theme.palette.common.blue}/>
                        </Button>
                    </Grid>
                    <Grid item>
                        <img src={mobileAppsIcon} alt="mobile icon" className={classes.icon}/>
                    </Grid>
                </Grid>

                <Grid container direction="row" justify={matchesSM ? "center" : undefined} className={classes.serviceContainer}>
                    {/* -- Website Development Block -- */}
                    <Grid item>
                        <Typography variant="h4">
                            Website Development
                        </Typography>
                        <Typography variant="subtitle1" className={classes.subtitle}>
                            Reach More. Discover More. Sell more.
                        </Typography>
                        <Typography variant="subtitle1">
                            Optimized for Search Engines,
                            <br />
                            built for speed.
                        </Typography>
                        <Button className={classes.learnButton} color={"primary"} variant={"outlined"} disableRipple>
                            <span style={{marginRight: 5}}>Learn More</span>
                            <ButtonArrow class="" width={15} height={15} fill={theme.palette.common.blue}/>
                        </Button>
                    </Grid>
                    <Grid item>
                        <img src={websiteIcon} alt="website icon" className={classes.icon}/>
                    </Grid>
                </Grid>
            </Grid>

            <Grid container justify="center">
                <Grid item className={classes.cardHolder}>
                    <Card>
                        <CardHeader title="The Revolution"/>
                        <CardContent>
                            <Typography variant="subtitle1" className={classes.subtitle}>
                                Visionary insights coupled with cutting-edge technology is a <br/>
                                recipe for revolution.
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>

        </Grid>
    );
}

export default LandingPage;